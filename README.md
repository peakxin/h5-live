# H5直播

## 可用的直播流地址

**1. RTMP协议直播源**
- 香港卫视：rtmp://live.hkstv.hk.lxdns.com/live/hks

**2. RTSP协议直播源**
- 珠海过澳门大厅摄像头监控：rtsp://218.204.223.237:554/live/1/66251FC11353191F/e7ooqwcfbqjoo80j.sdp
- 大熊兔（点播）：rtsp://184.72.239.149/vod/mp4://BigBuckBunny_175k.mov

**3. HTTP协议直播源**
- 香港卫视：http://live.hkstv.hk.lxdns.com/live/hks/playlist.m3u8
- CCTV1高清：http://ivi.bupt.edu.cn/hls/cctv1hd.m3u8
- CCTV3高清：http://ivi.bupt.edu.cn/hls/cctv3hd.m3u8
- CCTV5高清：http://ivi.bupt.edu.cn/hls/cctv5hd.m3u8
- CCTV5+高清：http://ivi.bupt.edu.cn/hls/cctv5phd.m3u8
- CCTV6高清：http://ivi.bupt.edu.cn/hls/cctv6hd.m3u8
- 苹果提供的测试源（点播）：http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear2/prog_index.m3u8

## HLS协议直播

报错：

```
Uncaught (in promise) DOMException: play() failed because the user didn't interact with the document first. https://goo.gl/xX8pDD
```

这是因为，Chrome只允许用户对网页进行主动触发后才可自动播放音频和视频。其实，严格地来说，是Chrome不允许在用户对网页进行触发之前播放音频，而视频其实是不受限制的。但因为视频文件同样包含了音频，所以也一同被禁止了。Chrome这样做的目的是为了防止开发者滥用自动播放功能而对用户产生骚扰。

既然知道了原因，那就开始找解决方法。

比较常规的做法是，为video标签设置muted属性，使它静音，这样视频就能自动播放了，但是没有声音:

```
<video src="YOUR_VIDEO_URL" autoplay muted></video>
```

然后待用户在网页上有了任意触发后，再将muted去掉，或者让用户手动去打开音频（腾讯视频就是这样做的）：

```
document.body.addEventListener('mousedown', function(){
    var vdo = $("video")[0]; //jquery
    vdo.muted = false;
}, false);  
```